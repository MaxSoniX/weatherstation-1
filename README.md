# WeatherStation-1
## Specification
- DevBoard [Blue Pill](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill.html) [STM32F103C8T6](https://www.st.com/content/st_com/en/products/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus/stm32-mainstream-mcus/stm32f1-series/stm32f103/stm32f103c8.html#overview)
- Debugger ST-LINK V2 [(Alt)](https://github.com/blacksphere/blackmagic/wiki)
- Humidity 3in1 sensor [BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/)
- DS18B20